import { generateContactId } from '@/utils/contact-utils';
import { getDefaultLabels } from '@/utils/defaults-utils';
import {Mutations} from '../mutations';
import {Actions} from '../actions';
import {Getters} from '../getters';

const state = {
  items: [],
  selectedItemId: 0,
  selectedContactsIds: [],
  availableLabels: [],
  showFavorites: false,
  filters: {
    labels: [],
    search: ''
  }
};

const getters = {
  [Getters.contacts.selectedContact]: (state) => {
    return state.items.find(c => c.id === state.selectedItemId);
  },
  [Getters.contacts.selectedContacts]: (state) => {
    return state.items.filter(c => state.selectedContactsIds.includes(c.id));
  },
  [Getters.contacts.filteredContacts]: (state) => {
    if (!state.filters.labels.length) {
      return state.items
        .filter(c => !state.showFavorites ? true : c.starred);
    }

    return state.items
      .filter(c => !state.showFavorites ? true : c.starred)
      .filter(c => c.labels.some(l => state.filters.labels.includes(l)));
  }
};

const mutations = {
  [Mutations.contacts.setShowFavoritesState](state, show) {
    state.showFavorites = show;
  },
  [Mutations.contacts.setFavoriteItemState](state, id) {
    state.items = [...state.items].map(c => {
      if (c.id === id) {
        c.starred = !c.starred;
      }

      return c;
    });
  },
  [Mutations.contacts.updateContacts](state, contacts) {
    state.items = [...contacts];
  },
  [Mutations.contacts.deleteSingleContact](state, id) {
    state.items = [...state.items].filter(c => c.id !== id);
  },
  [Mutations.contacts.deleteMultipleContacts](state, ids) {
    state.items = state.items.filter(c => !ids.includes(c.id));
  },
  [Mutations.contacts.deleteAllContacts](state) {
    state.items = [];
  },
  [Mutations.contacts.updateAvailableLabels](state, labels) {
    state.availableLabels = [...labels];
  },
  [Mutations.contacts.updateLabelFilter](state, labels) {
    state.filters.labels = [...labels];
  },
  [Mutations.contacts.updateSearchFilter](state, searchPhrase) {
    state.filters.search = searchPhrase;
  },
  [Mutations.contacts.setSelectedContactId](state, id) {
    state.selectedItemId = id;
  },
  [Mutations.contacts.addContact](state, contact) {
    state.items.push({ ...contact, id: generateContactId(state.items) });
  },
  [Mutations.contacts.updateContact](state, contact) {
    state.items = [...state.items].map(c => {
      if (c.id === contact.id) {
        return contact;
      }

      return c;
    });
  },
  [Mutations.contacts.saveContactsInLocalStorage](state) {
    localStorage.setItem('contacts-app-data', JSON.stringify(state.items));
  },
  [Mutations.contacts.initializeDefaultLabels](state) {
    if (!state.items.length) {
      state.availableLabels = [...getDefaultLabels()];
      return;
    }

    const labelsFromContacts = state.items
      .map(c => c.labels)
      .reduce((a, b) => a.concat(b));

    state.availableLabels = Array.from(new Set([...getDefaultLabels(), ...labelsFromContacts]));
  },
  [Mutations.contacts.setSelectedContacts](state, ids) {
    state.selectedContactsIds = [...ids];
  },
};

const actions = {
  [Actions.contacts.setShowFavoritesState]({ commit }, show) {
    commit(Mutations.contacts.setShowFavoritesState, show);
  },
  [Actions.contacts.setFavoriteItemState]({ commit }, id) {
    commit(Mutations.contacts.setFavoriteItemState, id);
  },
  [Actions.contacts.updateContacts]({ commit }, contacts) {
    commit(Mutations.contacts.updateContacts, contacts);
  },
  [Actions.contacts.deleteSingleContact]({ commit }, id) {
    commit(Mutations.contacts.deleteSingleContact, id);
  },
  [Actions.contacts.deleteMultipleContacts]({ commit }, ids) {
    commit(Mutations.contacts.deleteMultipleContacts, ids);
  },
  [Actions.contacts.deleteAllContacts]({ commit }) {
    commit(Mutations.contacts.deleteAllContacts);
  },
  [Actions.contacts.updateAvailableLabels]({ commit }, labels) {
    commit(Mutations.contacts.updateAvailableLabels, labels);
  },
  [Actions.contacts.updateLabelFilter]({ commit }, label) {
    commit(Mutations.contacts.updateLabelFilter, label);
  },
  [Actions.contacts.updateSearchFilter]({ commit }, searchPhrase) {
    commit(Mutations.contacts.updateSearchFilter, searchPhrase);
  },
  [Actions.contacts.setSelectedContactId]({ commit }, id) {
    commit(Mutations.contacts.setSelectedContactId, id);
  },
  [Actions.contacts.saveContact]({ commit }, contact) {
    if (contact.id) {
      commit(Mutations.contacts.updateContact, contact);
    }  else {
      commit(Mutations.contacts.addContact, contact);
    }
  },
  [Actions.contacts.saveContactsInLocalStorage]({ commit }) {
    commit(Mutations.contacts.saveContactsInLocalStorage);
  },
  [Actions.contacts.initializeDefaultLabels]({ commit }) {
    commit(Mutations.contacts.initializeDefaultLabels);
  },
  [Actions.contacts.setSelectedContacts]({ commit }, ids) {
    commit(Mutations.contacts.setSelectedContacts, ids);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
}
