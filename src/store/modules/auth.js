import { Actions } from '../actions';
import { Mutations } from '../mutations';

const state = {
  authenticated: false
};

const mutations = {
  [Mutations.auth.setAuthenticated](state, status) {
    state.authenticated = status;
  }
};

const actions = {
  [Actions.auth.setAuthenticated]({ commit }, status) {
    commit(Mutations.auth.setAuthenticated, status);
  }
};

export default {
  state,
  mutations,
  actions
}
