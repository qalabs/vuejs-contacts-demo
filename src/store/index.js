import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import contacts from './modules/contacts'
import snackbar from './modules/snackbar'

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    contacts,
    snackbar
  }
});

export default store;
