# Contacts App

## Online version

https://qalabs.gitlab.io/vuejs-contacts-demo/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Running local distribution with Maven

```
./mvnw jetty:run
```

Open your browser and navigate to `http://localhost:8080`