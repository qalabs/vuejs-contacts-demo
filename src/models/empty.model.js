export const Empty = {
  contact: {
    avatarUrl: '',
    name: '',
    email: '',
    phone: '',
    labels: [],
    notes: ''
  }
};
