export const getDefaultContacts = () =>{
  return [
    {
      id: 1,
      name: 'Fryderyk Maciejewski',
      email: 'fryderyk.maciejewski@teleworm.us',
      phone: '195 24 07 22',
      labels: ['Friends'],
      notes: '',
      starred: true,
      avatarUrl: ''
    },
    {
      id: 2,
      name: 'Julita Walczak',
      email: 'julita.walczak@rhyta.com',
      phone: '157 62 72 31',
      labels: ['Friends', 'Co-workers'],
      notes: '',
      starred: false,
      avatarUrl: ''
    },
    {
      id: 3,
      name: 'Emeryk Kaczmarek',
      email: 'emeryk.kaczmarek@rhyta.com',
      phone: '443 93 21 53',
      labels: ['Friends', 'Co-workers'],
      notes: '',
      starred: false,
      avatarUrl: ''
    }
  ];
};

export const getDefaultLabels = () => {
  return [
    'Friends',
    'Co-workers'
  ];
};

export const getDefaultAvatar = '//ssl.gstatic.com/s2/oz/images/sge/grey_silhouette.png';
