Contacts App Todo
=================

- Enable edit (navigate to edit), favorites and delete from details view
- New labels should be added to default labels, so later there can be reused
- Show selected labels in details view
- Multi-select in data-table to support multiple contacts removal (confirm with native confmirm dialog)
- Snackbar should be a generic service / component
- Show avatar in the list, detail etc.
- Filtering by label (click on the list to filter)
- Show favorites option
- Rethink about page - probably modal will be better
- Add home page with possibility to login with popup (logo + description)
- Componentize the application, remove duplicates, improve the source code
- Store data as JSON in localStorage
- Download contacts as CSV


## Optional

- Import contacts from CSV
- Signup and store user in localStorage
- Simulate email submission (click email) - basic WYSWIG editor
- Prepare for Firebase integration including auth (https://medium.com/@anas.mammeri/vue-2-firebase-how-to-build-a-vue-app-with-firebase-authentication-system-in-15-minutes-fdce6f289c3c)