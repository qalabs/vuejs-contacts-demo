export const generateContactId = (allContacts) => {
  const existingIds = allContacts.map((c) => c.id);

  return Math.max(...existingIds) + 1;
};
