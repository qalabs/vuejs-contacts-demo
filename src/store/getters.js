export const Getters = {
  contacts: {
    selectedContact: 'GET_SELECTED_CONTACT',
    selectedContacts: 'GET_SELECTED_CONTACTS',
    filteredContacts: 'GET_FILTERED_CONTACTS'
  }
};
