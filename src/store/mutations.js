export const Mutations = {
  auth: {
    setAuthenticated: 'SET_AUTHENTICATED'
  },
  snackbar: {
    showInfo: 'SHOW_INFO_SNACKBAR',
    showError: 'SHOW_ERROR_SNACKBAR',
    reset: 'RESET_SNACKBAR'
  },
  contacts: {
    setShowFavoritesState: 'SET_SHOW_FAVORITES_STATE',
    setFavoriteItemState: 'SET_FAVORITE_ITEM_STATE',
    updateContacts: 'UPDATE_CONTACTS',
    deleteSingleContact: 'DELETE_SINGLE_CONTACT',
    deleteMultipleContacts: 'DELETE_MULTIPLE_CONTACTS',
    deleteAllContacts: 'DELETE_ALL_CONTACTS',
    updateAvailableLabels: 'UPDATE_AVAILABLE_LABELS',
    updateLabelFilter: 'UPDATE_LABEL_FILTER',
    updateSearchFilter: 'UPDATE_SEARCH_FILTER',
    setSelectedContactId: 'SET_SELECTED_CONTACT_ID',
    addContact: 'ADD_CONTACT',
    updateContact: 'UPDATE_CONTACT',
    saveContactsInLocalStorage: 'SAVE_CONTACTS_IN_LOCAL_STORAGE',
    initializeDefaultLabels: 'INITIALIZE_DEFAULT_LABELS',
    setSelectedContacts: 'SET_SELECTED_CONTACTS'
  }
};
