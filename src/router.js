import Vue from 'vue'
import Router from 'vue-router'
import store from './store/index.js';
import Contact from './views/Contact'
import Contacts from './views/Contacts'
import Home from './views/Home';
import Login from './views/Login'
import Register from './views/Signup'

Vue.use(Router);

export default new Router({

    routes: [
        {
            path: '/',
            name: 'contacts',
            component: Contacts,
            beforeEnter: (to, from, next) => {
                if (store.state.auth.authenticated) {
                    next();
                } else {
                    next("/home")
                }
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        }
    ]
})
