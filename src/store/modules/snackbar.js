import {Mutations} from '../mutations';
import { Actions } from '../actions';

const state = {
  visible: false,
  text: '',
  colorClass: 'info',
};

const mutations = {
  [Mutations.snackbar.showInfo](state, text) {
    state.visible = true;
    state.text = text;
    state.colorClass = 'info';
  },
  [Mutations.snackbar.showError](state, text) {
    state.visible = true;
    state.text = text;
    state.colorClass = 'error';
  },
  [Mutations.snackbar.reset](state) {
    state.visible = false;
    state.text = '';
    state.colorClass = 'info';
  },
};

const actions = {
  [Actions.snackbar.showInfo]({ commit }, text) {
    commit(Mutations.snackbar.showInfo, text);
  },
  [Actions.snackbar.showError]({ commit }, text) {
    commit(Mutations.snackbar.showError, text);
  },
  [Actions.snackbar.reset]({ commit }) {
    commit(Mutations.snackbar.reset);
  }
};

export default {
  state,
  mutations,
  actions
}
